<div class="row  "></div>


<div class="row container ">
     <section class="col s12 m6 grey lighten-2">

           <article >


                
                <p class=" combinado flow-text right"> Assim como o processamento de dados que é a atividade onde ocorre uma entrada de dados e posteriosamente isso é processado para a obtenção de uma saída de informação.O processamento de imagem é quando um dado é uma imagem e sofre alguma
                    modificação para o resultado final ser outra imagem.</p>
                <p class="flow-text" > Por exemplo: Uma imagem colorida qualquer pode passa por uma modificação na cor e o resultado final seria outra imagem em escala cinza.</p>
                  
                <img class="responsive-img " src="_imagens/exemplo.png" alt=" sistema Barlane" >
                    <figcaption class="legenda flow-text   " > <em>figura ilustrativa </em> </figcaption>

            
                <h2 class="flow-text">Origem</h2>
                
                    
                
                    <ol class="flow-text">
                        <li> Indústria jornalística: transmissões de imagens via cabo submarino entre Londrese Nova Iorque (1920)</li>  
                        <li>Sistema de Bartlane usava 5 níveis, sendo expandido para 15 níveis em 1929 </li>
                        <li>Primeiros computadores poderosos o suficiente para fazer PDI surgiram nos anos 60</li>
                        <li>Jet Propulsion Laboratory trabalhou na restauração de imagens da lua transmitidas pela Ranger 7</li>
                        <li>No final dos anos 60 e início dos anos 70 PDI começou a ser utilizado em imagens médicas, astronômicas, etc </li>
                        <li> A partir daí a diversificação de aplicações é enorme, como em segurança,
                            entretenimento, metereologia, biologia geografia, geologia, engenharias, etc.</li>
                    </ol>
                <img class="responsive-img " src="_imagens/sistemaBarlane.png" alt=" sistema Barlane" >
                <figcaption class="legenda flow-text   " > <em>Umas das imagens que  utilizou o sistema de
                        Barlane  com 5 tons de cinza </em> </figcaption>
                    
            
                
               
                
                



                <h2 class="flow-text">Qual o objetivo?</h2>
                    <p class="flow-text">O processamento de imagens tem como funções facilitar a visualização da imagem ou adequá-la para análises quantitativas através de correções de defeitos ou realces das regiões de interesse nas imagens; e a extração e tratamento de dados
                    quantitativos, feitos pelo próprio computador. (Gomes, 2001). </p>

               
            
                
       
        
           


        </section>
<aside class="col s12 m5 offset-m1">
    <h2 class="flow-text">Etapas do processamento de imagens </h2>
        <ol class="flow-text"  >
            <li> Criação de imagem e obtenção de imagem;</li>
                <li> Digitalização;</li>
                <li>Pré-processamento;</li>
                <li>segmentação; </li>
                <li> pós-processamento; </li> 
                <li>extração de atributos; </li> 
                <li>classificação e reconhecimento. </li>
        </ol>
        <h2 class="flow-text"> Quais os passos necessário para digitalizar uma imagem?</h2>
                    <ol class="flow-text"> 
                        <li>Efetuar aquisição de uma imagem analógica;</li>
                        <li>Identificar a amostra;</li>
                        <li>Damos valor a amostra (Quantificação);</li>
                     <li>Digitalizamos a Quantificação;</li>
                    </ol>
            </article>
       
    
<h2> Links para Referências</h2> 
<a class="ref flow-text center "  href="https://www.maxwell.vrac.puc-rio.br/21365/21365_6.PDF" target="_blank">Puc Rio</a>
<br>
<a class="ref flow-text " href="https://pt.wikipedia.org/wiki/Processamento_de_imagem" target="_blank">Wikipedia</a>

        </aside>

       
</div>